/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    //console.log("ok");
  var check = [];
  check = sporocilo.split(' ');
 // console.log(check);
  if (check[1] == 'gggggggggggggggggggg')
    {
    var znak = '&#9758;';
    // var izpis = check[0] + ' (Zasebno): ' + znak + ' Omemba v klepetu';
    var izpis = check[0] + ' (Zasebno): ';
     // console.log(izpis);
      return $('<div style="font-weight: bold"></div>').text(izpis + "&#9758;"); 

    }else{
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
    }
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
 

 
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

//za youtube
function divElementYoutubeLink(sporocilo) {
  return $('<td></td>').html('<iframe src='+sporocilo+' allowfullscreen></iframe>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  var besede = [];
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    besede = sporocilo.split(' ');
    //console.log(besede);
    for (var i in besede){
      if (besede[i].charAt(0) == '@'){
       // console.log("yes");
        besede[i] = besede[i].replace('@','');
        var omemba = '/zasebno "' + besede[i] + '" "gggggggggggggggggggg"';
    //  console.log(omemba);
        klepetApp.procesirajUkaz(omemba);
        
      }
      //console.log(besede[i]);
    } 
  }

  $('#poslji-sporocilo').val('');
  var povezava = [];
  povezava = sporocilo.split(' ');
  //console.log(povezava);
  for (var i in povezava){
    var delBesede = [];
    delBesede = povezava[i].split('=');
   // console.log("del besede " + delBesede);
    for (var j in delBesede){
     // console.log(j + " " + delBesede[j]);
      if (delBesede[j] == 'https://www.youtube.com/watch?v'){
       povezava[i] = povezava[i].replace("watch?v=","embed/")
      //  console.log("povezava " +povezava[i]);
        $('#sporocila').append(divElementYoutubeLink(povezava[i]));
      }
    }
  }
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var preveriOmembo = sporocilo.besedilo.split(' ');
    var novElement;
  //  console.log("izpisi tekst " + sporocilo.besedilo + " " + preveriOmembo[1]);
    if (preveriOmembo[1] == 'gggggggggggggggggggg')
    {
      sporocilo.besedilo = preveriOmembo[0] + ' (zasebno): &#9758; Omemba v klepetu';
      novElement = divElementHtmlTekst(sporocilo.besedilo);
    }
    else if(preveriOmembo[1] == 'ffffffffffffffffffff')
    {
      sporocilo.besedilo = "Sporočila '&#9758; Omemba v klepetu' uporabniku z vzdevkom '" + preveriOmembo[0] +
      "' ni bilo mogoče posredovati";
      novElement = divElementHtmlTekst(sporocilo.besedilo);
    }
    else
    {
    novElement = divElementEnostavniTekst(sporocilo.besedilo);
    }
    $('#sporocila').append(novElement);
    var povezava = [];
  //  console.log("sporocilo " + sporocilo.besedilo);
  povezava = sporocilo.besedilo.split(' ');
 // console.log(povezava);
  for (var i in povezava)
  {
    var delBesede = [];
   // delBesede = povezava[i].split('/');
    //console.log("del besede " + delBesede);
    for (var j in delBesede)
    {
    //  console.log(j + " " + delBesede[j]);
      if (delBesede[j] == 'www.youtube.com')
      {
       povezava[i] = povezava[i].replace("watch?v=","embed/")
        //console.log("povezava " +povezava[i]);
        $('#sporocila').append(divElementYoutubeLink(povezava[i]));
      }
    }
  } 
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


/* global $, io, Klepet */